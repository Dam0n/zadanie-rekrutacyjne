import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Game } from '../shared/models/game.model';
import { tap, catchError } from 'rxjs/operators'
import { Observable, of } from "rxjs";

@Injectable({ providedIn: 'root' })
export class GameService {
    private baseUrl = 'http://localhost:3000/games';


    constructor(private httpClient: HttpClient) { }

getGames(){
   return this.httpClient.get<Game[]>(this.baseUrl).pipe(tap(games => {return games}), catchError(this.handleError('getGames', [])))
}


private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      console.error(error);
 
      console.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}