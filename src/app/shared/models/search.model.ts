export class Search {
    title: string;
    dateStart: string;
    dateEnd: string;
    sale: boolean;
    priceStart: number;
    priceEnd: number;
}