export class Game {
    id: number;
    title: string;
    imageURL: string;
    releaseDate: Date;
    price: number;
    sale: boolean;
    genre: string;
    platform: string;
    score: number;
    developer: string;
    publisher: string;
    requiredAge: number;
    language: string;
    controler: boolean;
    multiplayer: boolean;
  }