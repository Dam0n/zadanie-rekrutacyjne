import { Game } from './game.model';
import { Search } from './search.model';

export interface AppState {
    readonly games: Game[];
    readonly search: Search;
    readonly results: Game[];
    readonly selectedGame: Game;
    readonly sortedGames: Game[];
  }