import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '../environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { gameReducer } from './store/game.reducer';
import { FormsModule } from '@angular/forms';

import { routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { GamesListComponent } from './dashboard/games-list/games-list.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchComponent } from './dashboard/search/search.component';
import { GameEffects } from './store/game.effects';

@NgModule({
  declarations: [
    AppComponent,
    GamesListComponent,
    GameDetailComponent,
    DashboardComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    StoreModule.forRoot({
      game: gameReducer
    }),
    FormsModule,
    EffectsModule.forRoot([GameEffects]),
    StoreDevtoolsModule.instrument({
      name: 'Zadanie rekrutacyjne',
      logOnly: environment.production,
    }),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
