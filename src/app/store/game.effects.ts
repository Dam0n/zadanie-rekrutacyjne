import { Injectable } from "@angular/core";
import { GameService } from "../services/game.service"
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';

import * as types from './action.types'
import * as gamesActions from './game.actions';

@Injectable({
    providedIn: 'root'
})

export class GameEffects {
    constructor(private gameService: GameService, private actions$: Actions) { }

    @Effect() loadGames$: Observable<Action> = this.actions$.pipe(
        ofType<gamesActions.LoadGames>(types.LOAD_GAMES),
        mergeMap(() => this.gameService.getGames().pipe(
            map(games => (new gamesActions.LoadGamesComplete(games)))
        ))
    )

}