import * as GameActions from './game.actions';
import * as types from './action.types';
import { AppState } from '../shared/models/app.state';

// Section 1
const initialState: AppState = {
    games: [],
    search: {
        title: '',
        dateStart: "",
        dateEnd: "",
        sale: false,
        priceStart: null,
        priceEnd: null,
    },
    results: [],
    selectedGame: null,
    sortedGames: []

}

// Section 2
export function gameReducer(state = initialState, action: GameActions.Actions) {

    // Section 3
    switch (action.type) {
        case types.LOAD_GAMES:
            return {
                ...state
            }
        case types.LOAD_GAMES_COMPLETE:
            return {
                ...state, games: action.payload,
                results: action.payload,
                sortedGames: action.payload
            }
        case types.RESET_GAMES:
            return {
                ...state, results: state.games,
                sortedGames: state.games

            }
        case types.SEARCH_GAMES:
            return {
                ...state, search: action.payload

            }
        case types.SEARCH_GAMES_COMPLETE:
            return {
                ...state, results: action.payload,
                sortedGames: action.payload
            }
        case types.SEARCH_GAMES_RESET:
            return { ...state, search: initialState.search }

        case types.SORT_GAMES:
            return { ...state, sortedGames: action.payload }

        case types.SELECTED_GAME:
            return { ...state, selectedGame: action.payload }
        default:
            return state;
    }


}