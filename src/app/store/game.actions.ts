import { Action } from '@ngrx/store'
import { Game } from '../shared/models/game.model';
import { Search } from '../shared/models/search.model';
import * as types from './action.types';







export class LoadGames implements Action {
    readonly type = types.LOAD_GAMES
}
export class LoadGamesComplete implements Action {
    readonly type = types.LOAD_GAMES_COMPLETE

    constructor(public payload: Game[]) {}
}
export class ResetGames implements Action {
    readonly type = types.RESET_GAMES

}

export class SearchGames implements Action {
    readonly type = types.SEARCH_GAMES

    constructor(public payload: Search) {}
}
export class SearchGamesComplete implements Action {
    readonly type = types.SEARCH_GAMES_COMPLETE
    constructor(public payload: Game[]) {}
}
export class SearchGamesReset implements Action {
    readonly type = types.SEARCH_GAMES_RESET
}
export class SortGames implements Action {
    readonly type = types.SORT_GAMES
    constructor(public payload: Game[]) {}
}
export class SelectedGame implements Action {
    readonly type = types.SELECTED_GAME

    constructor(public payload: Game) {}
}




export type Actions = LoadGames | LoadGamesComplete | ResetGames | SearchGames | SearchGamesReset | SearchGamesComplete | SortGames | SelectedGame;