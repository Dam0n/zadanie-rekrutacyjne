import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { select } from '@ngrx/store';
import { Game } from '../../shared/models/game.model';
import { AppState } from '../../shared/models/app.state';
import { Router } from '@angular/router';
import { GameService } from '../../services/game.service';
import * as GameActions from '../../store/game.actions';


@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.css']
})
export class GamesListComponent implements OnInit {
  results$: Observable<Game[]>;
  results: Game[];
  selectedGame$: Observable<Game>;
  sortedGames: Game[];
  sortedGames$: Observable<Game[]>;
  orderByTitle: number;
  orderByDate: number;
  orderByPrice: number;


  constructor(private store: Store<AppState>, private router: Router) {

    this.results$ = this.store.pipe(select('game', 'results'));
    this.sortedGames$ = this.store.pipe(select('game', 'sortedGames'))
  }

  ngOnInit() {

    this.orderByDate = -1;
    this.orderByPrice = -1;
    this.orderByTitle = -1;
    this.getGames();
    this.results$.subscribe(results=> this.results = results);
    //brak potrzeby unsubscribe zapytan httpClient(skonczony strumien danych)


  }
  getGames() {
    this.store.dispatch(new GameActions.LoadGames());
  }

  onSelect(game: Game): void {
    this.store.dispatch({ type: 'SELECTED_GAME', payload: game });
    this.router.navigate(["/gamedetail"]);
  }

  onSortTitle(): void {
    this.orderByPrice = -1;
    this.orderByDate = -1;
    if (this.orderByTitle == -1) {
      this.orderByTitle = 0;
    }
    else if (this.orderByTitle == 0) {
      this.orderByTitle = 1;
    }
    else if (this.orderByTitle == 1) {
      this.orderByTitle = 0;
    }

    this.sortedGames = [...this.results].sort((obj1, obj2) => {

      if (this.orderByTitle == 0) {
        if (obj1.title > obj2.title) {
          return 1;
        }

        if (obj1.title < obj2.title) {
          return -1;
        }

        return 0;

      }
      else if (this.orderByTitle == 1) {
        if (obj1.title > obj2.title) {
          return -1;
        }

        if (obj1.title < obj2.title) {
          return 1;
        }

        return 0;
      }

    })

    this.store.dispatch(new GameActions.SortGames(this.sortedGames));

  }
  onSortDate(): void {
    this.orderByPrice = -1;
    this.orderByTitle = -1;

    if (this.orderByDate == -1) {
      this.orderByDate = 0;
    }
    else if (this.orderByDate == 0) {
      this.orderByDate = 1;
    }
    else if (this.orderByDate == 1) {
      this.orderByDate = 0;
    }

    this.sortedGames = [...this.results].sort((obj1, obj2) => {
      const tempReleaseDateObj1 = new Date(obj1.releaseDate);
      const tempReleaseDateObj2 = new Date(obj2.releaseDate);
      if (this.orderByDate == 0) {
        if (tempReleaseDateObj1 > tempReleaseDateObj2) {
          return 1;
        }

        if (tempReleaseDateObj1 < tempReleaseDateObj2) {
          return -1;
        }

        return 0;

      }
      else if (this.orderByDate == 1) {
        if (tempReleaseDateObj1 > tempReleaseDateObj2) {
          return -1;
        }

        if (tempReleaseDateObj1 < tempReleaseDateObj2) {
          return 1;
        }

        return 0;
      }

    })

    this.store.dispatch(new GameActions.SortGames(this.sortedGames));
  }




  onSortPrice(): void {
    this.orderByDate = -1;
    this.orderByTitle = -1;

    if (this.orderByPrice == -1) {
      this.orderByPrice = 0;
    }
    else if (this.orderByPrice == 1) {
      this.orderByPrice = 0;
    }

    else if (this.orderByPrice == 0) {
      this.orderByPrice = 1;
    }


    this.sortedGames = [...this.results].sort((obj1, obj2) => {
      if (this.orderByPrice == 0) {
        if (obj1.price > obj2.price) {
          return 1;
        }

        if (obj1.price < obj2.price) {
          return -1;
        }

        return 0;

      }
      else if (this.orderByPrice == 1) {
        if (obj1.price > obj2.price) {
          return -1;
        }

        if (obj1.price < obj2.price) {
          return 1;
        }

        return 0;
      }

    })
    this.store.dispatch(new GameActions.SortGames(this.sortedGames));
  }

}
