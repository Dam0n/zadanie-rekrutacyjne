import { Component, OnInit } from '@angular/core';
import { AppState } from 'src/app/shared/models/app.state';
import { Store } from '@ngrx/store';
import { Search } from '../../shared/models/search.model';
import { FormGroup } from '@angular/forms';
import { Game } from 'src/app/shared/models/game.model';




@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  search: Search;
  formSearch: FormGroup;
  gameList: Game[];
  results: Game[];

  constructor(private store: Store<AppState>) {
    this.store.select("game", "games").subscribe(data => this.gameList = data);
  }

  ngOnInit() { }

  onSubmit(searchForm) {
    this.formSearch = searchForm;
    this.search = searchForm.value;
    
    if (this.search.dateStart) {
      const tempDateSearch = this.search.dateStart.split("/", 3);
      this.search.dateStart = tempDateSearch[1] + "/" + tempDateSearch[0] + "/" + tempDateSearch[2];
    }
    if (this.search.dateEnd) {
      const tempDateSearch = this.search.dateEnd.split("/", 3);
      this.search.dateEnd = tempDateSearch[1] + "/" + tempDateSearch[0] + "/" + tempDateSearch[2];
    }

    this.store.dispatch({ type: 'SEARCH_GAMES', payload: this.search });

    this.results = this.gameList.filter(game => {

      let tempDateStart: Date;
      let tempDateEnd: Date;
      const tempReleaseDate = new Date(game.releaseDate);
      if (this.search.dateStart) {
        tempDateStart = new Date(this.search.dateStart);
      }
      if (this.search.dateEnd) {
        tempDateEnd = new Date(this.search.dateEnd);
      }

      return (!this.search.title || game.title.toLocaleLowerCase().includes(this.search.title.toLocaleLowerCase()))
        && (!this.search.sale || game.sale == this.search.sale)
        && (!this.search.priceStart || game.price >= this.search.priceStart)
        && (!this.search.priceEnd || game.price <= this.search.priceEnd)
        && (!this.search.dateStart || tempReleaseDate >= tempDateStart)
        && (!this.search.dateEnd || tempReleaseDate <= tempDateEnd);
    })
    this.store.dispatch({ type: 'SEARCH_GAMES_COMPLETE', payload: this.results });
  }
  onReset() {
    if (this.formSearch) {
      this.formSearch.reset();
    }
    this.store.dispatch({ type: 'RESET_GAMES' });
    this.store.dispatch({ type: 'SEARCH_GAMES_RESET' });


  }

}
