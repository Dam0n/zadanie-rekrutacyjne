import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Game } from '../shared/models/game.model';
import { AppState } from '../shared/models/app.state';
import { Router } from '@angular/router';


@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit, AfterViewChecked {
  $selectedGame: Observable<Game>;

  constructor(private store: Store<AppState>, private router: Router) {
    this.$selectedGame = this.store.select('game', 'selectedGame');
  }

  ngOnInit() {

  }

  onBack() {
    this.router.navigate(["/dashboard"]);
  }

  ngAfterViewChecked() {
    this.$selectedGame.subscribe(data => {
      if (!data) {
        this.router.navigate(["/dashboard"]);
      }
    });
  }
}
