# Zadanie rekrutacyjne

Aplikacja ma wyświtlać gry komputerowe oraz pokazywać ich szczegóły. Użytkownik będzie mógł też filtrować i sortować produkty. Ponadto każda gra powinna mieć okładkę (w „bazie” może to być link do pliku), tytuł, datę wydania, cenę, czy jest aktualnie promocja oraz zawierać dodatkowo 10 innych danych, tak aby wyświetlić je w szczegółach produktu na nowej stronie.

Filtrowanie:
- szukajka po tytule (przykład: „Fifa” filtr powinien wyświetlić wszystkie jej wydania)
- zakres daty wydania (przykład: od 01.03.2018 do 01.12.2018)
- czy promocja (przykład: zaznaczam checkbox)
- zakres ceny (przykład: od 1zł do 99zł)

sortowanie:
- po tytule produktu,
- po cenie

Dla uproszczenia
Mockujemy backend wykorzystując bibliotekę json-server.
Filtrowanie i sortowanie odbywać się będzie po stronie frontendu.

Technologie:
Angular 7,
ngrx,
json-server,